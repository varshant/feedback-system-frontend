import React, { Component } from "react";
import getFeedbackRequests from "../api-calls/feedbackRequest/getFeedbackRequests";
import Spinner from "./Spinner";
import Interview from "./Interview";
import { Link } from "react-router-dom";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import deleteFeedbackRequest from "../api-calls/feedbackRequest/deleteFeedbackRequest";
import getFeedback from "../api-calls/feedback/getFeedback";
import getName from "../api-calls/user/getName";
class InterviewRequests extends Component {
  state = {
    spinner: true,
    requests: [],
    view: false,
    title: "",
    data: {
      applicantName: "",
      date: "",
      workExperience: {
        years: 0,
        months: 0
      },
      applyingFor: "",
      skills: [
        {
          name: "",
          rating: 0
        }
      ],
      comments: "",
      status: "",
      resumePath: ""
    }
  };
  confirm = (id, assignedBy) => {
    confirmAlert({
      message: "Are you sure to remove this assignment by " + assignedBy,
      buttons: [
        {
          label: "Yes",
          onClick: () => this.rejectInterviewRequest(id)
        },
        {
          label: "No",
          onClick: () => {}
        }
      ]
    });
  };
  rejectInterviewRequest = id => {
    this.setState({ spinner: true });
    deleteFeedbackRequest(id, this.props.token).then(res => {
      if (res.message === "deleted") {
        let requestsCopy = [];
        requestsCopy = this.state.requests.filter(request => {
          if (request._id != id) return request;
        });
        this.setState({ spinner: false, requests: requestsCopy });
        this.props.handleNotification("Deleted Successfully");
      }
    });
  };
  componentDidMount() {
    getFeedbackRequests(this.props.id, this.props.token).then(res => {
      if (res.message === "successful") {
        this.setState({ requests: res.doc, spinner: false });
      }
    });
  }
  viewInterview = id => {
    if (this.props.type === "admin") {
      let data = this.props.applicants.filter(feedback => {
        if (feedback._id === id) return feedback;
      });
      let title = this.props.activeInterviewers.filter(interviewer => {
        if (data[0].interviewerId === interviewer.value) return interviewer;
      });
      this.setState({ data: data[0], view: true, title: title[0].label });
    } else if (this.props.type === "interviewer") {
      getFeedback(id, this.props.token).then(res => {
        if (res.message === "successful") {
          if (res.doc.interviewerId === this.props.id) {
            this.setState({
              data: res.doc,
              view: true,
              title: "Me"
            });
          } else {
            getName(res.doc.interviewerId, this.props.token).then(result => {
              if (result.message === "successful") {
                this.setState({
                  data: res.doc,
                  view: true,
                  title: result.doc.name
                });
              }
            });
          }
        }
      });
    }
  };
  toggle = () => {
    this.setState({ view: !this.state.view });
  };
  render() {
    return (
      <div>
        <Interview
          view={this.state.view}
          toggle={this.toggle}
          data={this.state.data}
          title={this.state.title}
          token={this.props.token}
          activeInterviewers={
            this.props.type === "admin" ? this.props.activeInterviewers : null
          }
          handleApplicantDelete={msg => this.props.handleNotification(msg)}
          showAssigned={false}
        />
        {this.state.spinner ? (
          <Spinner />
        ) : (
          <div className=" active-list col-sm-12 col-lg-offset-1">
            {this.state.requests.length > 0 ? (
              <div>
                <thead className=" col-sm-12 interviewer-row">
                  <th className="col-sm-3">
                    <label>Assigned By</label>
                  </th>
                  <th className="col-sm-3">
                    <label>Applicant Name</label>
                  </th>
                  <th className="col-sm-3">
                    <label>Old Feedback</label>
                  </th>
                  <th className="col-sm-3">
                    <label>Actions</label>
                  </th>
                </thead>
                {this.state.requests.map(request => (
                  <tr className=" col-sm-12 interviewer-row2">
                    <td className="col-sm-3">{request.assignedBy}</td>
                    <td className="col-sm-3">{request.applicantName}</td>
                    <td className="col-sm-3">
                      <span
                        onClick={() =>
                          this.viewInterview(request.oldInterviewId)
                        }
                        className="old-interview-icon"
                      >
                        <i class="fa fa-eye " aria-hidden="true" />
                      </span>
                    </td>
                    <td className="col-sm-3">
                      <Link
                        to={{
                          pathname: "/" + this.props.type + "/feedback",
                          state: {
                            requestId: request._id,
                            oldInterviewId: request.oldInterviewId
                          }
                        }}
                      >
                        <span className="request-feedback">Give Feedback</span>
                      </Link>
                      <span
                        className="interview-reject"
                        onClick={() =>
                          this.confirm(request._id, request.assignedBy)
                        }
                      >
                        <i class="fas fa-times" />
                      </span>
                    </td>
                  </tr>
                ))}
              </div>
            ) : (
              <h3 className="empty">No pending requests</h3>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default InterviewRequests;
