import React, { Component } from "react";
import changePassword from "../api-calls/user/changePassword";
import Spinner from "./Spinner";
class ChangePassword extends Component {
  state = {
    oldPassword: "",
    newPassword: "",
    confirmNewPassword: "",
    message: "",
    spinner: false
  };
  captureChange = (e, name) => {
    this.setState({ [name]: e.target.value });
  };
  changePassword = e => {
    e.preventDefault();
    if (this.state.newPassword.length < 8) {
      this.setState({ message: "Password must contain atleast 8 characters" });
    } else if (this.state.newPassword != this.state.confirmNewPassword) {
      this.setState({ message: "New password mismatch" });
    } else {
      this.setState({ spinner: true });
      let data = {
        oldPassword: this.state.oldPassword,
        newPassword: this.state.newPassword
      };
      let det = JSON.stringify(data);
      changePassword(this.props.id, det, this.props.token).then(res => {
        if (res.message === "successful") {
          this.setState({
            newPassword: "",
            confirmNewPassword: "",
            oldPassword: "",
            spinner: false
          });
          this.props.handleNotification("password changed");
        } else if (res.message === "wrong old password") {
          this.setState({ message: "Wrong old password", spinner: false });
        }
      });
    }
  };
  render() {
    return (
      <div>
        {this.state.spinner ? (
          <Spinner />
        ) : (
          <form onSubmit={this.changePassword}>
            <div className="active-list col-sm-9 col-lg-offset-4">
              <table className="table change-password-table">
                <tr>
                  <td>
                    <label>Old password</label>
                  </td>
                  <td>
                    <input
                      type="password"
                      className="add-field"
                      onChange={e => this.captureChange(e, "oldPassword")}
                      value={this.state.oldPassword}
                      required
                    />
                  </td>
                </tr>
                <tr>
                  <td>
                    <label>New password</label>
                  </td>
                  <td>
                    <input
                      className="add-field "
                      onChange={e => this.captureChange(e, "newPassword")}
                      value={this.state.newPassword}
                      required
                    />
                  </td>
                </tr>
                <tr>
                  <td>
                    <label>Confirm new password</label>
                  </td>
                  <td>
                    <input
                      type="password"
                      className="add-field"
                      onChange={e =>
                        this.captureChange(e, "confirmNewPassword")
                      }
                      value={this.state.confirmNewPassword}
                      required
                    />
                  </td>
                </tr>
              </table>
            </div>
            <p className="error-message change-password-div">
              {this.state.message}
            </p>
            <div className="change-password-div">
              {" "}
              <input
                type="password"
                type="submit"
                className="change-password-button"
                value="Change password"
              />
            </div>
          </form>
        )}
      </div>
    );
  }
}

export default ChangePassword;
