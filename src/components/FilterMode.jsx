import React, { Component } from "react";
class FilterMode extends Component {
  state = {};
  render() {
    return (
      <div className="row filter-mode">
        <span className="col-sm-4 see-border" />

        <label className="col-sm-1">Filter By</label>
        {/* <span className="col-sm-1" /> */}
        <label className="radio-inline col-sm-2 see-border option1">
          <input
            type="radio"
            name="mode"
            value="interviewer"
            checked={this.props.filterMode === "interviewer"}
            onChange={this.props.handleChange}
            className="radio-border radio"
          />
          Interviewer's Name
        </label>

        <label className="radio-inline col-sm-2 see-border option2">
          <input
            type="radio"
            name="mode"
            value="applicant"
            checked={this.props.filterMode === "applicant"}
            onChange={this.props.handleChange}
            className="radio-border radio"
          />
          Applicant's name
        </label>
      </div>
    );
  }
}

export default FilterMode;
