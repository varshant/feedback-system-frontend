import React, { Component } from "react";
import { Redirect, withRouter, Route, Link } from "react-router-dom";
import Admin from "./Admin";
import UserHome from "./UserHome";
import MessageComponent from "./MessageComponent";
class Signin extends Component {
  state = {};
  componentDidMount() {
    // this.props.disableSignupDone();
  }
  componentDidUpdate() {
    // if (this.props.navigate === "admin" && this.props.auth) {
    //   this.props.history.push("/admin");
    //   console.log(this.props.history);
    // } else if (this.props.navigate === "interviewer" && this.props.auth) {
    //   this.props.history.push("/interviewer");
    // }
  }
  render() {
    return (
      <div className="first-background">
        {/* <MessageComponent message={this.props.notificationMsg} /> */}

        <div className=" container">
          <h3
            style={{
              textAlign: "center",
              marginTop: "70px",
              fontSize: "40px"
            }}
          >
            Feedback System
          </h3>
          <div className=" interviewer-details-form col-lg-6 col-lg-offset-3 ">
            <form onSubmit={this.props.handleLogin}>
              <div className="form-group row  col-lg-offset-3">
                <div className="col-sm-8 field">
                  <i className="fa fa-envelope fa-2x icon" aria-hidden="true" />

                  <input
                    type="email"
                    className="text-field"
                    placeholder="Enter your email"
                    required
                  />
                </div>
              </div>
              <div className="form-group row col-lg-offset-3">
                <div className="col-sm-8  field">
                  <i class="fas fa-key fa-2x" />
                  <input
                    type="password"
                    className="text-field"
                    placeholder="Enter your password"
                    required
                  />
                </div>
              </div>
              <div className="form-group row col-lg-offset-3 ">
                <div className="col-sm-8 login-button ">
                  <input
                    type="submit"
                    className="proceed-button"
                    value="Login"
                  />
                </div>
              </div>
              <div className="form-group row col-lg-offset-3">
                <div className="col-sm-8  signup ">
                  <Link to={"/signup"}>Signup</Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Signin);
