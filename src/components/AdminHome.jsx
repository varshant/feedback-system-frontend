import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import Interview from "./Interview";
import getFeedbacks from "../api-calls/feedback/getFeedbacks";
import deleteFeedback from "../api-calls/feedback/deleteFeedback";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import Spinner from "./Spinner";
import Filter from "./Filter";
import InterviewCard from "./InterviewCard";
import checkFeedbackRequest from "../api-calls/feedbackRequest/checkFeedbackRequest";

class AdminHome extends Component {
  state = {
    view: false,
    title: "",
    data: {
      applicantName: "",
      date: "",
      workExperience: {
        years: 0,
        months: 0
      },
      applyingFor: "",
      skills: [
        {
          name: "",
          rating: 0
        }
      ],
      comments: "",
      status: "",
      resumePath: ""
    },
    auth: true,
    spinner: false,
    assignedTo: ""
  };
  static getDerivedStateFromProps(props, state) {}
  captureFilter = arr => {
    this.setState({ selected: arr });
  };
  toggle = () => {
    this.setState({ view: !this.state.view });
  };
  confirm = (id, name) => {
    confirmAlert({
      message: "Are you sure to delete feedback of " + name,
      buttons: [
        {
          label: "Yes",
          onClick: () => this.deleteInterview(id)
        },
        {
          label: "No",
          onClick: () => {}
        }
      ]
    });
  };
  deleteInterview = id => {
    this.setState({ spinner: true });
    deleteFeedback(id, this.props.token).then(res => {
      if (res.message === "deleted") {
        if (this.props.filterMode === "applicant") {
          this.props.handleApplicantDelete(id);
        }
        this.props.search();
        this.props.handleNotification("Feedback deleted");
        this.setState({ spinner: false });
      }
    });
  };
  viewInterview = (data, title) => {
    if (data.status != "Shortlisted for second round") {
      this.setState({
        data: data,
        view: true,
        title: title,
        spinner: false
      });
    } else {
      this.setState({ spinner: true });
      checkFeedbackRequest(data._id, this.props.token).then(res => {
        if (res.message === "found") {
          let assignedTo = this.props.activeInterviewers.filter(interviewer => {
            if (res.doc.newInterviewerId === interviewer.value)
              return interviewer;
          });
          this.setState({
            data: data,
            view: true,
            title: title,

            assignedTo: assignedTo[0].label,
            spinner: false
          });
        } else if (res.message === "not found") {
          this.setState({
            data: data,
            view: true,
            title: title,
            assignedTo: "",
            spinner: false
          });
        }
      });
    }
  };

  // search = () => {
  //   this.setState({ spinner: true, feedbacks: [] });
  //   if (this.state.filterMode === "interviewer") {
  //     let feedbacksCopy = new Array(this.state.selected.length);
  //     feedbacksCopy.fill([]);

  //     if (this.state.selected.length === 0) {
  //       this.setState({ feedbacks: [], spinner: false });
  //     } else {
  //       let names = [];
  //       this.state.selected.map((interviewer, index) => {
  //         names.push(interviewer.label);
  //         feedbacksCopy[index] = this.state.applicants.filter(applicant => {
  //           if (interviewer.value === applicant.interviewerId) {
  //             return applicant;
  //           }
  //         });
  //         this.setState({
  //           feedbacks: feedbacksCopy,
  //           interviewerNames: names,
  //           spinner: false
  //         });
  //         // getFeedbacks(interviewer.value, this.props.token).then(res => {
  //         //   if (res.message !== "auth failed") {
  //         //     feedbacksCopy[index] = res;

  //         //     // if (index === this.state.selected.length - 1) {
  //         //     this.setState({
  //         //       feedbacks: feedbacksCopy,
  //         //       interviewerNames: names,
  //         //       spinner: false
  //         //     });
  //         //     // }
  //         //   } else {
  //         //     feedbacksCopy = [];
  //         //   }
  //         // });
  //       });
  //     }
  //   } else if (this.state.filterMode === "applicant") {
  //     let feedbacksCopy = new Array(this.state.selected.length);
  //     feedbacksCopy.fill([]);
  //     let names = [];
  //     if (this.state.selected.length === 0) {
  //       this.setState({ feedbacks: [], spinner: false });
  //     } else {
  //       this.state.selected.map((applicant, index) => {
  //         this.state.applicants.map(data => {
  //           if (
  //             applicant.label === data.applicantName &&
  //             applicant.value === data._id
  //           ) {
  //             this.state.activeInterviewers.map(interviewer => {
  //               if (interviewer.value === data.interviewerId) {
  //                 console.log(interviewer.label);
  //                 names.push(interviewer.label);
  //               }
  //             });
  //             feedbacksCopy[index] = [data];
  //           }
  //           this.setState({
  //             feedbacks: feedbacksCopy,
  //             interviewerNames: names,
  //             spinner: false
  //           });
  //         });
  //       });
  //     }
  //   }
  // };
  // handleApplicantDelete = id => {
  //   let optionsCopy = [];
  //   this.state.options.map(option => {
  //     if (option.value !== id) {
  //       optionsCopy.push(option);
  //     }
  //   });

  //   let applicantsCopy = [];
  //   this.state.applicants.map(applicant => {
  //     if (applicant._id !== id) {
  //       applicantsCopy.push(applicant);
  //     }
  //   });

  //   let selectedCopy = [];
  //   this.state.selected.map(applicant => {
  //     if (applicant.value !== id) {
  //       selectedCopy.push(applicant);
  //     }
  //   });
  //   this.setState({
  //     selected: selectedCopy,
  //     options: optionsCopy,
  //     applicants: applicantsCopy
  //   });
  // };
  render() {
    return (
      <div>
        {this.state.spinner ? (
          <Spinner />
        ) : this.state.auth ? (
          <div>
            <div className=" home-feedback">
              <Interview
                view={this.state.view}
                toggle={this.toggle}
                data={this.state.data}
                title={this.state.title}
                token={this.props.token}
                activeInterviewers={this.props.activeInterviewers}
                handleApplicantDelete={msg =>
                  this.props.handleNotification(msg)
                }
                name={this.props.name}
                showAssigned={true}
                assignedTo={this.state.assignedTo}
                handleNotification={msg => this.props.handleNotification(msg)}
                showChangeStatus={true}
                refresh={() => this.props.search()}
              />

              {this.props.feedbacks.map((interviewer, index) => {
                return (
                  <InterviewCard
                    interviewer={interviewer}
                    name={this.props.interviewerNames[index]}
                    viewInterview={data =>
                      this.viewInterview(
                        data,
                        this.props.interviewerNames[index]
                      )
                    }
                    confirm={(id, applicantName) =>
                      this.confirm(id, applicantName)
                    }
                  />
                );
              })}
            </div>
          </div>
        ) : (
          <Redirect to="/" />
        )}
      </div>
    );
  }
}

export default AdminHome;
