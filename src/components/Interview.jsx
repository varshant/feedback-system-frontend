import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import Rating from "./Rating";
import editFeedback from "../api-calls/feedback/editFeedback";
import getResume from "../api-calls/feedback/getResume";
import addFeedbackRequest from "../api-calls/feedbackRequest/addFeedbackRequest";
class Interview extends Component {
  state = { assignedInterviewerId: "", status: "" };
  captureChangeStatus = (e, name) => {
    this.setState({ [name]: e.target.value });
  };
  download = () => {
    getResume(this.props.data._id, this.props.token);
  };
  captureChange = e => {
    let index = e.target.selectedIndex - 1;
    this.setState({
      assignedInterviewerId: this.props.activeInterviewers[index].value
    });
  };
  assignInterviewer = () => {
    let data = {
      oldInterviewId: this.props.data._id,
      newInterviewerId: this.state.assignedInterviewerId,
      assignedBy: this.props.name,
      applicantName: this.props.data.applicantName
    };

    addFeedbackRequest(JSON.stringify(data), this.props.token).then(res => {
      if (res.message === "feedback request sent") {
        this.setState({ assignedInterviewerId: "" });
        this.props.toggle();
        this.props.handleNotification("feedback request sent");
      }
    });
  };
  changeStatus = () => {
    let data = { status: this.state.status };
    editFeedback(
      this.props.data._id,
      JSON.stringify(data),
      this.props.token
    ).then(res => {
      if (res.message === "successful") {
        this.props.toggle();
        this.props.refresh();
        this.props.handleNotification("Status changed successfully");
      }
    });
  };
  componentDidMount() {
    this.setState({ assignedInterviewerId: "", status: "" });
  }
  render() {
    return (
      <Modal
        show={this.props.view}
        onHide={() => {
          this.setState({ assignedInterviewerId: "" });
          this.props.toggle();
        }}
        bsSize="large"
        dialogClassName="my-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-sm">
            Interview by {this.props.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <div className=" border modal-back">
              <div className="applicant-name">
                {" "}
                {this.props.data.applicantName}
              </div>
              <div className="feedback-date">
                Date:{" "}
                {this.props.data.date
                  .substring(0, 10)
                  .split("-")
                  .reverse()
                  .join("-")}
              </div>
              <div className="feedback-details">
                <table className="table">
                  <tbody>
                    <tr>
                      <td className="info-type col-sm-3">Applying for:</td>
                      <td>{this.props.data.applyingFor}</td>
                    </tr>
                    <tr>
                      <td className="info-type col-sm-3">Work Experience: </td>
                      <td>
                        <span className="experience-space">
                          {this.props.data.workExperience.years} Y
                        </span>

                        <span className="experience-space">
                          {this.props.data.workExperience.months} M
                        </span>
                      </td>
                    </tr>

                    <tr>
                      <td className="info-type col-sm-3 ">Skills:</td>
                      <td>
                        {this.props.data.skills.map(skill => (
                          <tr>
                            <td>{skill.name}</td>
                            <td className="rating-td">
                              {[...Array(skill.rating).keys()].map(() => {
                                return (
                                  <i className="fas fa-star filled-star" />
                                );
                              })}
                              {[...Array(5 - skill.rating).keys()].map(() => {
                                return <i className="fas fa-star empty-star" />;
                              })}
                            </td>
                          </tr>
                        ))}
                      </td>
                    </tr>
                    <tr>
                      <td className="info-type col-sm-3"> Comments:</td>
                      <td>{this.props.data.comments}</td>
                    </tr>
                    <tr>
                      <td className="info-type col-sm-3"> Status:</td>
                      <td>{this.props.data.status}</td>
                    </tr>
                    {this.props.data.status ===
                      "Shortlisted for second round" &&
                    this.props.showAssigned ? (
                      this.props.assignedTo === "" ? (
                        <tr>
                          <td className="info-type col-sm-3">
                            Assign Interviewer:
                          </td>
                          <td>
                            {" "}
                            <select
                              onChange={e => this.captureChange(e)}
                              className="form-control drop feedback-field"
                              required
                            >
                              <option disabled value selected>
                                {" "}
                                Select an option{" "}
                              </option>

                              {this.props.activeInterviewers.map(i => {
                                return <option>{i.label} </option>;
                              })}
                            </select>
                          </td>
                          <td>
                            <input
                              type="button"
                              className="assign"
                              value="Assign"
                              disabled={
                                this.state.assignedInterviewerId.length === 0
                                  ? true
                                  : false
                              }
                              onClick={this.assignInterviewer}
                            />
                          </td>
                        </tr>
                      ) : (
                        <tr>
                          <td className="info-type col-sm-3">Assigned to:</td>
                          <td> {this.props.assignedTo}</td>
                        </tr>
                      )
                    ) : null}
                    {this.props.showChangeStatus &&
                    this.props.data.status === "On hold" ? (
                      <tr>
                        <td className="info-type col-sm-3">Change Status:</td>
                        <td>
                          <label className="radio-inline status-label">
                            <input
                              type="radio"
                              name="status"
                              value="Rejected"
                              checked={this.state.status === "Rejected"}
                              onChange={e =>
                                this.captureChangeStatus(e, "status")
                              }
                              class="radio-border"
                            />
                            Rejected
                          </label>
                          <label className="radio-inline status-label">
                            <input
                              type="radio"
                              name="status"
                              value="Selected"
                              checked={this.state.status === "Selected"}
                              onChange={e =>
                                this.captureChangeStatus(e, "status")
                              }
                              class="radio-border"
                            />
                            Selected
                          </label>
                          <input
                            type="button"
                            className="assign"
                            value="Change"
                            disabled={
                              this.state.status.length === 0 ? true : false
                            }
                            onClick={this.changeStatus}
                          />
                        </td>
                      </tr>
                    ) : null}
                    <tr>
                      <td className="info-type col-sm-3">Resume:</td>
                      <td>
                        <a
                          rel="noopener noreferrer"
                          href={
                            "https://protected-brook-37812.herokuapp.com/feedbacks/resume/" +
                            this.props.data._id +
                            ".pdf"
                          }
                          target="_blank"
                        >
                          <i class="fas fa-download fa-2x" />
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

export default Interview;
