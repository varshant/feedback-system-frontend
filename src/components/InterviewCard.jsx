import React, { Component } from "react";
import CardDetails from "./CardDetails";
class InterviewCard extends Component {
  state = {};
  static getDerivedStateFromProps(props, state) {
    return {
      name: props.name,
      interviewer: props.interviewer
    };
  }
  render() {
    return (
      <div>
        <div className="row name-tag">
          Feedbacks by <span className="name">{this.state.name}</span>
        </div>
        <div className="form-group row row-overflow">
          {this.state.interviewer.length === 0 ? (
            <span className="no-feedback">No feedback available</span>
          ) : (
            this.state.interviewer.map((data, i) => {
              return (
                <CardDetails
                  data={data}
                  viewInterview={() => this.props.viewInterview(data)}
                  confirm={() =>
                    this.props.confirm(data._id, data.applicantName)
                  }
                />
              );
            })
          )}
        </div>
      </div>
    );
  }
}

export default InterviewCard;
