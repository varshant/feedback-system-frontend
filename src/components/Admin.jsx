import React, { Component } from "react";
import Navbar from "./Navbar";
import { Router, Route, Redirect } from "react-router-dom";
import Feedback from "./Feedback";
import UserHome from "./UserHome";
import ActiveInterviewers from "./ActiveInterviewers";
import PendingRequests from "./PendingRequests";
import getActiveInterviewers from "../api-calls/user/getActiveInterviewers";
import Filter from "./Filter";
import getFeedbacks from "../api-calls/feedback/getFeedbacks";
import AdminHome from "./AdminHome";
import Logout from "./Logout";
import CommonComponent from "./CommonComponent";
import FilterMode from "./FilterMode";
import Spinner from "./Spinner";
import InterviewRequests from "./InterviewRequests";
import ChangePassword from "./ChangePassword";
class Admin extends Component {
  state = {
    filterMode: "",
    FilterOptions: [],
    activeInterviewers: [],
    options: [],
    applicants: [],
    spinner: false,
    feedbacks: [],
    activeInterviewersOptions: [],
    showUserRequests: true,
    showInterviewRequests: false,
    auth: true
  };

  componentDidMount() {
    this.fetchAllData();
  }
  fetchAllData = () => {
    getActiveInterviewers(
      this.props.approverId,
      this.props.id,
      this.props.token
    )
      .then(res => {
        let activeInterviewersCopy = [];
        if (res.message === "auth failed") {
          this.setState({ auth: false });
          this.props.clearStorage();
        } else {
          res.map(data => {
            activeInterviewersCopy.push({ label: data.name, value: data._id });
          });
          activeInterviewersCopy.unshift({ label: "Me", value: this.props.id });
          // this.setState({ activeInterviewersOptions: activeInterviewersCopy });
        }
        return activeInterviewersCopy;
      })
      .then(activeInterviewers => {
        let optionsCopy = [];
        let applicantfeedbacksCopy = [];
        let promises = activeInterviewers.map(data => {
          return getFeedbacks(data.value, this.props.token);
        });
        Promise.all(promises)
          .then(result => {
            result.map(feedbacks => {
              feedbacks.map(feedback => {
                optionsCopy.push({
                  label: feedback.applicantName,
                  value: feedback._id
                });
              });

              applicantfeedbacksCopy = applicantfeedbacksCopy.concat(feedbacks);
            });
            return {
              applicantfeedbacksCopy: applicantfeedbacksCopy,
              optionsCopy: optionsCopy,
              activeInterviewers: activeInterviewers
            };
          })
          .then(data =>
            this.setState({
              applicants: data.applicantfeedbacksCopy,
              applicantsOption: data.optionsCopy,
              activeInterviewersOptions: data.activeInterviewers,
              spinner: false
            })
          )
          .then(() => this.search());
      });
  };
  captureFilter = arr => {
    this.setState({ selected: arr });
  };
  handleChange = e => {
    this.setState({
      filterMode: e.target.value,
      selected: [],
      feedbacks: []
    });
    if (e.target.value === "interviewer") {
      this.setState({ options: this.state.activeInterviewersOptions });
    } else if (e.target.value === "applicant") {
      this.setState({ options: this.state.applicantsOption });
    }
  };
  search = () => {
    this.setState({ feedbacks: [] });

    if (this.state.filterMode === "interviewer") {
      let feedbacksCopy = new Array(this.state.selected.length);
      feedbacksCopy.fill([]);

      if (this.state.selected.length === 0) {
        this.setState({ feedbacks: [], spinner: false });
      } else {
        let names = [];
        this.state.selected.map((interviewer, index) => {
          names.push(interviewer.label);
          feedbacksCopy[index] = this.state.applicants.filter(applicant => {
            if (interviewer.value === applicant.interviewerId) {
              return applicant;
            }
          });
          this.setState({
            feedbacks: feedbacksCopy,
            interviewerNames: names,
            spinner: false
          });
        });
      }
    } else if (this.state.filterMode === "applicant") {
      let feedbacksCopy = new Array(this.state.selected.length);
      feedbacksCopy.fill([]);
      let names = [];
      if (this.state.selected.length === 0) {
        this.setState({ feedbacks: [], spinner: false });
      } else {
        this.state.selected.map((applicant, index) => {
          this.state.applicants.map(data => {
            if (
              applicant.label === data.applicantName &&
              applicant.value === data._id
            ) {
              this.state.activeInterviewersOptions.map(interviewer => {
                if (interviewer.value === data.interviewerId) {
                  console.log(interviewer.label);
                  names.push(interviewer.label);
                }
              });
              feedbacksCopy[index] = [data];
            }
            this.setState({
              feedbacks: feedbacksCopy,
              interviewerNames: names,
              spinner: false
            });
          });
        });
      }
    }
  };
  handleApplicantDelete = id => {
    let optionsCopy = [];
    this.state.options.map(option => {
      if (option.value !== id) {
        optionsCopy.push(option);
      }
    });

    let applicantsCopy = [];
    this.state.applicants.map(applicant => {
      if (applicant._id !== id) {
        applicantsCopy.push(applicant);
      }
    });

    let selectedCopy = [];
    this.state.selected.map(applicant => {
      if (applicant.value !== id) {
        selectedCopy.push(applicant);
      }
    });
    this.setState({
      selected: selectedCopy,
      options: optionsCopy,
      applicants: applicantsCopy
    });
  };
  filterRequest = type => {
    if (type === "user") {
      this.setState({ showUserRequests: true, showInterviewRequests: false });
    } else {
      this.setState({ showUserRequests: false, showInterviewRequests: true });
    }
  };
  render() {
    return (
      <div className="admin-div">
        {this.state.auth ? null : <Redirect to="/" />}
        <Navbar name={this.props.name} />

        <div className="details">
          <Route
            path="/admin/admin-home"
            render={() => {
              return (
                <div>
                  {/* <h3 className="admin-details-heading">
                    Feedbacks given so far...
                  </h3> */}

                  <Logout handleLogout={this.props.handleLogout} />
                  <h3 className="admin-details-heading ">Feedbacks</h3>

                  <FilterMode
                    handleChange={this.handleChange}
                    filterMode={this.state.filterMode}
                  />
                  <div className="row filter">
                    <span className="col-sm-3" />
                    <div className="col-sm-6">
                      <Filter
                        captureFilter={this.captureFilter}
                        options={this.state.options}
                        selected={this.state.selected}
                      />
                    </div>
                    <span className="col-sm-3 ">
                      <input
                        type="button"
                        onClick={() => {
                          this.fetchAllData();
                          this.setState({
                            spinner: true
                          });
                        }}
                        value="Search"
                        className="feedback-search"
                      />
                    </span>
                  </div>
                  {this.state.spinner ? <Spinner /> : null}
                  <AdminHome
                    name={this.props.name}
                    interviewerId={this.props.id}
                    interviewerEmail={this.props.email}
                    token={this.props.token}
                    applicants={this.state.applicants}
                    activeInterviewers={this.state.activeInterviewersOptions}
                    handleNotification={msg =>
                      this.props.handleNotification(msg)
                    }
                    filterMode={this.state.filterMode}
                    handleApplicantDelete={id => {
                      this.handleApplicantDelete(id);
                    }}
                    search={() => this.fetchAllData()}
                    feedbacks={this.state.feedbacks}
                    options={this.state.options}
                    interviewerNames={this.state.interviewerNames}
                  />
                </div>
              );
            }}
          />
          <Route
            path="/admin/view/user-home"
            render={() => {
              return (
                <div>
                  <h3 className="admin-details-heading">
                    Feedbacks given so far...
                  </h3>

                  <UserHome
                    interviewerName={this.props.name}
                    interviewerId={this.props.id}
                    interviewerEmail={this.props.email}
                    token={this.props.token}
                  />
                </div>
              );
            }}
          />

          <Route
            path="/admin/active-interviewers"
            render={() => {
              return (
                <div className="fixed">
                  <Logout handleLogout={this.props.handleLogout} />
                  <h3 className="admin-details-heading">Active Users</h3>
                  <div className="container">
                    <ActiveInterviewers
                      id={this.props.id}
                      token={this.props.token}
                      approverId={this.props.approverId}
                      handleNotification={msg =>
                        this.props.handleNotification(msg)
                      }
                    />
                  </div>
                </div>
              );
            }}
          />
          <Route
            path="/admin/pending-requests"
            render={() => {
              return (
                <div>
                  <Logout handleLogout={this.props.handleLogout} />

                  <h3 className="admin-details-heading">Pending Requests</h3>

                  <div className="container">
                    <div className="request-filter-div">
                      <input
                        type="button"
                        value="User"
                        className={
                          this.state.showUserRequests
                            ? "request-filter-active"
                            : "request-filter-inactive"
                        }
                        onClick={() => this.filterRequest("user")}
                      />
                      <input
                        type="button"
                        value="Interview"
                        className={
                          this.state.showInterviewRequests
                            ? "request-filter-active"
                            : "request-filter-inactive"
                        }
                        onClick={() => this.filterRequest("interview")}
                      />
                    </div>
                    {this.state.showUserRequests ? (
                      <PendingRequests
                        id={this.props.id}
                        approverId={this.props.approverId}
                        token={this.props.token}
                        handleNotification={msg =>
                          this.props.handleNotification(msg)
                        }
                      />
                    ) : (
                      <InterviewRequests
                        token={this.props.token}
                        id={this.props.id}
                        applicants={this.state.applicants}
                        activeInterviewers={
                          this.state.activeInterviewersOptions
                        }
                        handleNotification={msg =>
                          this.props.handleNotification(msg)
                        }
                        type="admin"
                      />
                    )}
                  </div>
                </div>
              );
            }}
          />
          <Route
            path="/admin/feedback"
            render={props => {
              return (
                <div>
                  <Logout handleLogout={this.props.handleLogout} />
                  <h3 className="admin-details-heading">Feedback Form</h3>
                  <Feedback
                    search={() => this.fetchAllData()}
                    oldInterviewId={
                      props.location.state
                        ? props.location.state.oldInterviewId
                        : null
                    }
                    requestId={
                      props.location.state
                        ? props.location.state.requestId
                        : null
                    }
                    interviewerName={this.props.name}
                    interviewerId={this.props.id}
                    interviewerEmail={this.props.email}
                    token={this.props.token}
                    handleNotification={msg =>
                      this.props.handleNotification(msg)
                    }
                    type="admin"
                    refresh={() => this.fetchAllData}
                  />
                </div>
              );
            }}
          />
          <Route
            path="/admin/skills"
            render={() => {
              return (
                <div>
                  <Logout handleLogout={this.props.handleLogout} />
                  <h3 className="admin-details-heading">Skills</h3>
                  <div className="container">
                    <CommonComponent
                      interviewerId={this.props.id}
                      token={this.props.token}
                      collection="skills"
                      handleNotification={msg =>
                        this.props.handleNotification(msg)
                      }
                    />
                  </div>
                </div>
              );
            }}
          />
          <Route
            path="/admin/designations"
            render={() => {
              return (
                <div>
                  <Logout handleLogout={this.props.handleLogout} />
                  <h3 className="admin-details-heading">Designations</h3>
                  <CommonComponent
                    interviewerId={this.props.id}
                    token={this.props.token}
                    collection="designations"
                    handleNotification={msg =>
                      this.props.handleNotification(msg)
                    }
                  />
                </div>
              );
            }}
          />
          <Route
            path="/admin/edit-profile"
            render={() => {
              return (
                <div>
                  <Logout handleLogout={this.props.handleLogout} />
                  <h3 className="admin-details-heading">Change Password</h3>
                  <ChangePassword
                    id={this.props.id}
                    token={this.props.token}
                    handleNotification={msg =>
                      this.props.handleNotification(msg)
                    }
                  />
                </div>
              );
            }}
          />
        </div>
      </div>
    );
  }
}

export default Admin;
