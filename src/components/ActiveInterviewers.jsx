import React, { Component } from "react";
import getActiveInterviewers from "../api-calls/user/getActiveInterviewers";
import { Link } from "react-router-dom";
import deleteUser from "../api-calls/user/deleteUser";
import deleteApprover from "../api-calls/approver/deleteApprover";
import deleteFeedbacks from "../api-calls/feedback/deleteFeedbacks";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import Spinner from "./Spinner";
class ActiveInterviewers extends Component {
  state = { auth: true, activeList: [], spinner: true };
  componentDidMount() {
    getActiveInterviewers(
      this.props.approverId,
      this.props.id,
      this.props.token
    ).then(res => {
      console.log(res);
      if (res.message !== "auth failed") {
        this.setState({ activeList: res, spinner: false });
      }
    });
  }
  confirm = (id, index, role, email, name) => {
    confirmAlert({
      message: "Are you sure to remove " + name,
      buttons: [
        {
          label: "Yes",
          onClick: () => this.removeInterviewer(id, index, role, email)
        },
        {
          label: "No",
          onClick: () => {}
        }
      ]
    });
  };
  removeInterviewer = (id, index, role, email) => {
    this.setState({ spinner: true });
    if (role === "admin") {
      deleteApprover(email, this.props.token).then(res => {
        if (res.message === "auth failed") {
          this.setState({ auth: false });
        }
      });
    }
    deleteFeedbacks(id, this.props.token).then(res => {
      if (res.message === "auth failed") {
        this.setState({ auth: false });
      }
    });
    deleteUser(id, this.props.token).then(res => {
      if (res.message !== "auth failed") {
        getActiveInterviewers(
          this.props.approverId,
          this.props.id,
          this.props.token
        ).then(res => {
          console.log(res);
          if (res.message !== "auth failed") {
            this.setState({ activeList: res, spinner: false });
          }
        });
      }
      this.props.handleNotification("removed");
    });
  };
  render() {
    return (
      <div>
        {this.state.spinner ? (
          <Spinner />
        ) : (
          <div className=" active-list col-sm-12 col-lg-offset-1">
            {this.state.activeList.length > 0 ? (
              <div>
                <thead className=" col-sm-12 interviewer-row">
                  <th className="col-sm-3">
                    <label>Name</label>
                  </th>

                  <th className="col-sm-4">
                    <label>Email</label>
                  </th>

                  <th className="col-sm-3">
                    <label>Role</label>
                  </th>

                  <th className="col-sm-2">
                    <label>Actions</label>
                  </th>
                </thead>
                {this.state.activeList.map((i, index) => (
                  <tr className="col-sm-12 interviewer-row2">
                    <td className="col-sm-3">{i.name}</td>

                    <td className="col-sm-4">{i.email}</td>

                    <td className="col-sm-3">{i.requestType}</td>

                    <td className="col-sm-2">
                      <input
                        type="button"
                        value="Remove"
                        className="remove-button"
                        onClick={() =>
                          this.confirm(i._id, index, i.role, i.email, i.name)
                        }
                      />
                    </td>
                  </tr>
                ))}
              </div>
            ) : (
              <h3 className="empty">No active user</h3>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default ActiveInterviewers;
