import React, { Component } from "react";

import { BrowserRouter as Router, Link, NavLink } from "react-router-dom";

class Navbar extends Component {
  render() {
    return (
      <div className="sidebar-div">
        <div className="user">
          <i class="fa fa-user fa-2x" aria-hidden="true" />
          <br />
          {this.props.name}
        </div>

        <NavLink
          activeClassName="active-class"
          to="/admin/admin-home"
          style={{ textDecoration: "none" }}
        >
          <div className="link">Feedbacks</div>
        </NavLink>
        <NavLink
          to={"/admin/pending-requests"}
          activeClassName="active-class"
          style={{ textDecoration: "none" }}
        >
          <div className="link">Requests</div>
        </NavLink>
        <NavLink
          to={"/admin/active-interviewers"}
          activeClassName="active-class"
          style={{ textDecoration: "none" }}
        >
          <div className="link">Users</div>
        </NavLink>

        <NavLink
          to={"/admin/skills"}
          activeClassName="active-class"
          style={{ textDecoration: "none" }}
        >
          <div className="link">Skills</div>
        </NavLink>

        <NavLink
          to={"/admin/designations"}
          activeClassName="active-class"
          style={{ textDecoration: "none" }}
        >
          <div className="link">Designations</div>
        </NavLink>

        <NavLink
          to={"/admin/feedback"}
          activeClassName="active-class"
          style={{ textDecoration: "none" }}
        >
          <div className="link">Give feedback</div>
        </NavLink>
        <NavLink
          to={"/admin/edit-profile"}
          activeClassName="active-class"
          style={{ textDecoration: "none" }}
        >
          <div className="link">Edit Profile</div>
        </NavLink>
      </div>
    );
  }
}

export default Navbar;
