import React, { Component } from "react";
import Select from "react-select";

class Skills extends Component {
  state = {};
  render() {
    return (
      <div>
        <Select
          onChange={this.props.captureSkills}
          options={this.props.availableSkills}
          isMulti="true"
          isSearchable="true"
          placeholder="Search and add skills to rate..."
          className="multi-select"
          closeMenuOnSelect={false}
        />
      </div>
    );
  }
}

export default Skills;
