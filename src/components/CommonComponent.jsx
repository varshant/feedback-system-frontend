import React, { Component } from "react";
import deleteOption from "../api-calls/common/deleteOption";
import addOption from "../api-calls/common/addOption";
import fetchOptions from "../api-calls/common/fetchOptions";
import { confirmAlert } from "react-confirm-alert";

import Spinner from "./Spinner";
class CommonComponent extends Component {
  state = {
    skills: [],
    designations: [],
    field: "",
    spinner: true
  };
  componentDidMount() {
    fetchOptions(this.props.collection, this.props.token).then(res => {
      if (res.message === "success") {
        if (this.props.collection === "skills")
          this.setState({ skills: res.doc, spinner: false });
        else if (this.props.collection === "designations") {
          this.setState({ designations: res.doc, spinner: false });
        }
      }
    });
  }
  add = e => {
    e.preventDefault();
    this.setState({ spinner: true });
    let data = { name: e.target[0].value };
    addOption(
      JSON.stringify(data),
      this.props.token,
      this.props.collection
    ).then(res => {
      if (res.message === "added") {
        if (this.props.collection === "skills") {
          let skillsCopy = this.state.skills;
          skillsCopy.push(res.doc);
          this.setState({ skills: skillsCopy, spinner: false });
        } else {
          let designationsCopy = this.state.designations;
          designationsCopy.push(res.doc);
          this.setState({ designations: designationsCopy, spinner: false });
        }
      } else {
        this.setState({ spinner: false });
      }

      this.props.handleNotification(res.message);
    });
  };

  confirm = (id, name) => {
    confirmAlert({
      message: "Are you sure to remove " + name,
      buttons: [
        {
          label: "Yes",
          onClick: () => this.remove(id)
        },
        {
          label: "No",
          onClick: () => {}
        }
      ]
    });
  };
  remove = id => {
    this.setState({ spinner: true });
    deleteOption(id, this.props.token, this.props.collection).then(res => {
      if (res.message === "deleted") {
        if (this.props.collection === "skills") {
          let skillsCopy = [];
          this.state.skills.map(skill => {
            if (skill._id !== id) {
              skillsCopy.push(skill);
            }
          });
          this.setState({ skills: skillsCopy, spinner: false });
        } else if (this.props.collection === "designations") {
          let designationsCopy = [];
          this.state.designations.map(designation => {
            if (designation._id !== id) {
              designationsCopy.push(designation);
            }
          });
          this.setState({ designations: designationsCopy, spinner: false });
        }
      }
      this.props.handleNotification("Deleted Successfully");
    });
  };
  render() {
    return (
      <div>
        {this.state.spinner ? (
          <Spinner />
        ) : (
          <div className="active-list col-sm-6 col-sm-offset-4">
            <form onSubmit={this.add}>
              <span className="col-sm-10 add-row">
                <div className="form-group row">
                  <input
                    type="text"
                    className="col-sm-9 add-field"
                    required
                    placeholder="Enter new field"
                  />

                  <span className="col-sm-3">
                    <input type="submit" value="Add" className="add-button" />
                  </span>
                </div>
              </span>
            </form>

            {this.props.collection === "skills" ? (
              this.state.skills.length > 0 ? (
                <div>
                  <thead
                    className="col-sm-10 interviewer-row"
                    style={{ padding: "0" }}
                  >
                    {/* <div className="form-group row interviewer-row"> */}
                    <th className="col-sm-9">
                      <label>Name</label>
                    </th>

                    <th className="col-sm-3">
                      <label>Actions</label>
                    </th>
                    {/* </div> */}
                  </thead>
                  {this.state.skills.map(item => (
                    <tr
                      className="col-sm-10 interviewer-row2"
                      style={{ padding: "0" }}
                    >
                      {/* <div className="form-group row interviewer-row2"> */}
                      <td className="col-sm-9">{item.name}</td>

                      <td
                        className="col-sm-1"
                        onClick={() => this.confirm(item._id, item.name)}
                      >
                        <i class="far fa-trash-alt delete-item" />
                      </td>
                      {/* </div> */}
                    </tr>
                  ))}
                </div>
              ) : (
                <span className="col-sm-8" style={{ padding: "0" }}>
                  <h3 className="empty">Skills not available</h3>
                </span>
              )
            ) : this.state.designations.length > 0 ? (
              <div>
                <thead
                  className="col-sm-10 interviewer-row"
                  style={{ padding: "0" }}
                >
                  {/* <div className="form-group row interviewer-row"> */}
                  <th className="col-sm-9">
                    <label>Name</label>
                  </th>

                  <th className="col-sm-3">
                    <label>Actions</label>
                  </th>
                  {/* </div> */}
                </thead>
                {this.state.designations.map(item => (
                  <tr
                    className="col-sm-10 interviewer-row2"
                    style={{ padding: "0" }}
                  >
                    {/* <div className="form-group row interviewer-row2"> */}
                    <td className="col-sm-9">{item.name}</td>

                    <td
                      className="col-sm-1"
                      onClick={() => this.confirm(item._id, item.name)}
                    >
                      <i class="far fa-trash-alt delete-item" />
                    </td>
                    {/* </div> */}
                  </tr>
                ))}
              </div>
            ) : (
              <span className="col-sm-7" style={{ padding: "0" }}>
                <h3 className="empty">Designations not available</h3>
              </span>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default CommonComponent;
