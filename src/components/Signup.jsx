import React, { Component } from "react";
import { Redirect, withRouter, Link } from "react-router-dom";
import getApprovers from "../api-calls/approver/getApprovers";
class Signup extends Component {
  state = {
    approvers: []
  };
  componentDidMount() {
    getApprovers().then(res => this.setState({ approvers: res }));
  }
  render() {
    return (
      <div className="first-background">
        <div className=" container">
          <h3
            style={{ textAlign: "center", marginTop: "30px", fontSize: "40px" }}
          >
            Feedback System
          </h3>
          <div className=" signup-form col-lg-6 col-lg-offset-3 ">
            <h3 />
            <form onSubmit={this.props.handleSignup}>
              <div className="form-group row col-lg-offset-3">
                <div className="col-sm-8 field">
                  <i className="fa fa-user fa-2x icon " aria-hidden="true" />

                  <input
                    type="text"
                    className="text-field"
                    placeholder="Enter name"
                    required
                  />
                </div>
              </div>
              <div className="form-group row col-lg-offset-3">
                <div className="col-sm-8  field">
                  <i class="fa fa-envelope fa-2x icon" aria-hidden="true" />
                  <input
                    type="email"
                    className="text-field"
                    placeholder="Enter email"
                    required
                  />
                </div>
              </div>
              <div className="form-group row col-lg-offset-3">
                <div className="col-sm-8  field">
                  <i class="fas fa-key fa-2x" />
                  <input
                    type="password"
                    className="text-field"
                    placeholder="Create password"
                    required
                  />
                </div>
              </div>

              <div className="form-group row col-lg-offset-3">
                <div className="col-sm-8  field">
                  <span className="approver-icon">
                    <i class="fas fa-user-check" />
                  </span>
                  <select
                    className="form-control  text-field signup-select"
                    onChange={e => this.props.handleChange(e, "approverId")}
                  >
                    <option disabled value selected>
                      {" "}
                      Select approver{" "}
                    </option>

                    {this.state.approvers.map(data => {
                      return (
                        <option value={data.approverId}>{data.name} </option>
                      );
                    })}
                  </select>
                </div>
              </div>
              <div class="form-group row col-lg-offset-3">
                <div className="col-sm-8  field">
                  <label>Request Type</label>
                  <label className="radio-inline ">
                    <input
                      type="radio"
                      name="status"
                      value="interviewer"
                      checked={this.props.requestType === "interviewer"}
                      onChange={e => this.props.handleChange(e, "requestType")}
                      class="radio-border"
                    />
                    Interviewer
                  </label>
                  <label className="radio-inline ">
                    <input
                      type="radio"
                      name="status"
                      value="admin"
                      checked={this.props.requestType === "admin"}
                      onChange={e => this.props.handleChange(e, "requestType")}
                      class="radio-border"
                    />
                    Admin
                  </label>
                </div>
              </div>
              <div className="form-group row col-lg-offset-3">
                <div className="col-sm-8 login-button">
                  <input
                    type="submit"
                    className="signup-button"
                    value="Signup"
                    onClick={this.next}
                  />
                </div>
              </div>
              <div className="form-group row col-lg-offset-3">
                <div className="col-sm-8  signup ">
                  <Link to={"/"}>Signin</Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Signup);
