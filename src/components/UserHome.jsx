import React, { Component } from "react";
import getFeedbacks from "../api-calls/feedback/getFeedbacks";
import { Link, Redirect } from "react-router-dom";
import Interview from "./Interview";
import deleteFeedback from "../api-calls/feedback/deleteFeedback";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import CardDetails from "./CardDetails";
class UserHome extends Component {
  state = {
    feedbacks: [],
    view: false,
    data: {
      applicantName: "",
      date: "",
      workExperience: {
        years: 0,
        months: 0
      },
      applyingFor: "",
      skills: [
        {
          name: "",
          rating: 0
        }
      ],
      comments: "",
      status: "",
      resumePath: ""
    },
    auth: true
  };

  componentDidMount() {
    getFeedbacks(this.props.interviewerId, this.props.token).then(res => {
      if (res.message === "auth failed") {
        this.setState({
          auth: false
        });
        this.props.clearStorage();
      }
      if (res.message !== "auth failed") {
        this.setState({ feedbacks: res, auth: true });
      } else {
        // this.setState({ feedbacks: [], auth: false });
      }
    });
  }
  toggle = () => {
    this.setState({ view: !this.state.view });
  };
  confirm = (id, name) => {
    confirmAlert({
      message: "Are you sure to delete feedback of " + name,
      buttons: [
        {
          label: "Yes",
          onClick: () => this.deleteInterview(id)
        },
        {
          label: "No",
          onClick: () => {}
        }
      ]
    });
  };
  deleteInterview = id => {
    deleteFeedback(id, this.props.token).then(res => {
      if (res.message === "deleted") {
        let feedbacksCopy = [];
        this.state.feedbacks.map(feedback => {
          if (feedback._id !== id) {
            feedbacksCopy.push(feedback);
          }
        });
        this.setState({ feedbacks: feedbacksCopy });

        this.props.handleNotification("Feedback deleted");
      }
    });
  };
  viewInterview = data => {
    this.setState({ data: data, view: true });
  };
  render() {
    return (
      <div>
        {this.state.auth ? (
          <div className="home-feedback">
            <Interview
              view={this.state.view}
              toggle={this.toggle}
              data={this.state.data}
              title={this.props.interviewerName}
            />
            <div className="form-group row ">
              {this.state.feedbacks.map(data => {
                return (
                  <CardDetails
                    data={data}
                    viewInterview={() => this.viewInterview(data)}
                    confirm={() => this.confirm(data._id, data.applicantName)}
                  />
                );
              })}
            </div>
          </div>
        ) : (
          <Redirect to="/" />
        )}
      </div>
    );
  }
}

export default UserHome;
