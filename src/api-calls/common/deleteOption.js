import url from "../url";

var result = async function deleteItem(id, token, collection) {
  const res = await fetch(
    url + collection + "/" + id,

    {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
