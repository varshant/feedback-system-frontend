import url from "../url";

var result = async function declineRequest(id, token) {
  const res = await fetch(
    url + "users/request/" + id,

    {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
