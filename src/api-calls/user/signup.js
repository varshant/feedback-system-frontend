import url from "../url";

var result = async function signup(data) {
  const res = await fetch(
    url + "users/signup",

    {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json"
      },
      body: data
    }
  );
  const response = await res.json();
  return response;
};

export default result;
