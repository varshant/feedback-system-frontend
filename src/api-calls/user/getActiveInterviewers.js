import url from "../url";

var result = async function getActiveRequests(approverId, id, token) {
  const res = await fetch(
    url + "users/active/" + approverId + "/" + id,

    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
