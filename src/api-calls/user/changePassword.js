import url from "../url";

var result = async function changePassword(id, details, token) {
  const res = await fetch(
    url + "users/change-password/" + id,

    {
      method: "PUT",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: details
    }
  );

  const data = await res.json();

  return data;
};

export default result;
