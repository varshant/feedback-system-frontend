import url from "../url";
var result = async function addApprover(data, token) {
  const res = await fetch(
    url + "admins/",

    {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: data
    }
  );
  const response = await res.json();
  return response;
};

export default result;
