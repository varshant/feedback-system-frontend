import url from "../url";

var result = async function editFeedback(id, details, token) {
  const res = await fetch(
    url + "feedbacks/" + id,

    {
      method: "PUT",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: details
    }
  );

  const data = await res.json();

  return data;
};

export default result;
