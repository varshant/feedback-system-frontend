import url from "../url";

var result = async function deleteFeedbacks(id, token) {
  const res = await fetch(
    url + "feedbacks/" + id,

    {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
