import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  Switch
} from "react-router-dom";
import history from "./history";
import Signin from "./components/Signin";
import Signup from "./components/Signup";
import loginVerification from "./api-calls/user/loginVerification";
import signup from "./api-calls/user/signup";
import Admin from "./components/Admin";
import UserHome from "./components/UserHome";
import Interviewer from "./components/Interviewer";
import MessageComponent from "./components/MessageComponent";
import Spinner from "./components/Spinner";
class App extends Component {
  state = {
    name: "",
    email: "",
    adminRedirect: false,
    interviewerRedirect: false,
    requestType: "",
    navigate: "",
    auth: false,
    notificationMsg: "",
    signupDone: false,
    spinner: false
  };
  componentDidMount() {
    if (localStorage.getItem("navigate")) {
      if (localStorage.getItem("navigate") === "admin") {
        this.setState({
          notificationMsg: localStorage.getItem("notificationMsg"),
          signupDone: false,
          auth: localStorage.getItem("auth"),
          navigate: localStorage.getItem("navigate"),
          name: localStorage.getItem("name"),
          token: localStorage.getItem("token"),
          email: localStorage.getItem("email"),
          id: localStorage.getItem("id"),
          adminRedirect: localStorage.getItem("adminRedirect"),
          approverId: localStorage.getItem("approverId")
        });
      } else {
        this.setState({
          notificationMsg: localStorage.getItem("notificationMsg"),
          signupDone: false,
          auth: localStorage.getItem("auth"),
          navigate: localStorage.getItem("navigate"),
          token: localStorage.getItem("token"),
          name: localStorage.getItem("name"),
          email: localStorage.getItem("email"),
          id: localStorage.getItem("id"),
          adminRedirect: localStorage.getItem("interviewerRedirect")
        });
      }
    }
  }

  handleSignup = e => {
    e.preventDefault();
    this.setState({ spinner: true, notificationMsg: "" });
    let name = e.target[0].value;
    let email = e.target[1].value;
    let password = e.target[2].value;
    let approverId = this.state.approverId;
    let requestType = this.state.requestType;
    let data = {
      name: name,
      email: email,
      password: password,
      requestType: requestType,
      status: "pending",
      approverId: approverId,
      pendingWith: ""
    };
    signup(JSON.stringify(data)).then(res => {
      if (res.message === "signup successful") {
        this.setState({
          signupDone: true,
          spinner: false,
          notificationMsg: "signup successful"
        });
      }
    });
  };
  // disableSignupDone = () => {
  //   console.log("disabled");
  //   this.setState({ signupDone: false });
  // };
  handleLogin = e => {
    e.preventDefault();
    this.setState({ spinner: true, notificationMsg: "" });
    let email = e.target[0].value;
    let password = e.target[1].value;
    let data = { email: email, password: password };
    loginVerification(JSON.stringify(data)).then(res => {
      this.setState({ spinner: false });
      console.log(res.message);
      if (res.message === "email doesn't exist") {
        this.setState({
          auth: false,
          notificationMsg: "Email doesn't exist"
        });
      } else if (res.message === "wrong password") {
        this.setState({
          auth: false,
          notificationMsg: "Wrong Password"
        });
      } else if (res.message === "pending") {
        this.setState({
          auth: false,
          notificationMsg: "Pending",
          pendingWith: res.name
        });
      } else if (
        res.message === "auth successful" &&
        res.payload.userType === "admin"
      ) {
        localStorage.setItem("email", email);
        localStorage.setItem("name", res.payload.name);
        localStorage.setItem("id", res.payload.userId);
        localStorage.setItem("approverId", res.payload.approverId);
        localStorage.setItem("token", res.token);
        localStorage.setItem("adminRedirect", true);
        localStorage.setItem("navigate", "admin");
        localStorage.setItem("auth", true);
        localStorage.setItem("notificationMsg", "");
        this.setState(
          {
            email: email,
            name: res.payload.name,
            id: res.payload.userId,
            approverId: res.payload.approverId,
            token: res.token,
            adminRedirect: true,
            navigate: "admin",
            auth: true,
            notificationMsg: ""
          },
          () => this.redirect()
        );
      } else if (
        res.message === "auth successful" &&
        res.payload.userType === "interviewer"
      ) {
        localStorage.setItem("email", email);
        localStorage.setItem("name", res.payload.name);
        localStorage.setItem("id", res.payload.userId);

        localStorage.setItem("token", res.token);
        localStorage.setItem("interviewerRedirect", true);
        localStorage.setItem("navigate", "interviewer");
        localStorage.setItem("auth", true);
        localStorage.setItem("notificationMsg", "");
        this.setState(
          {
            email: email,
            name: res.payload.name,
            id: res.payload.userId,

            token: res.token,
            interviewerRedirect: true,
            navigate: "interviewer",
            auth: true,
            notificationMsg: ""
          },
          () => this.redirect()
        );
      } else {
        this.setState({
          auth: false,
          notificationMsg: "login error",
          spinner: false
        });
      }
    });
  };
  redirect = () => {};
  captureChange = (e, name) => {
    this.setState({ [name]: e.target.value, notificationMsg: "" });
  };
  handleLogout = () => {
    localStorage.clear();
    this.setState({ token: "", auth: false, notificationMsg: "logged out" });
  };
  clearStorage = () => {
    localStorage.clear();
    this.setState({ token: "", auth: false });
  };
  handleNotification = msg => {
    this.setState({ notificationMsg: msg });
  };
  render() {
    var a;
    window.onbeforeunload = function(e) {
      a = setTimeout(function() {
        window.location.href = "https://feedback-system.netlify.com";
      }, 200);
    };
    window.onunload = function() {
      clearTimeout(a);
    };
    return (
      // <div>
      //   {this.state.login ? (
      //     <Signin
      //       handleLogin={e => this.handleLogin(e)}
      //       navigate={this.state.navigate}
      //     />
      //   ) : this.state.navigate === "admin" ? (
      //     <Admin
      //       name={this.state.name}
      //       id={this.state.id}
      //       email={this.state.email}
      //       token={this.state.token}
      //     />
      //   ) : (
      //     <UserHome
      //       interviewerName={this.state.name}
      //       interviewerId={this.state.id}
      //       interviewerEmail={this.state.email}
      //       token={this.state.token}
      //     />
      //   )}
      // </div>
      <div>
        <MessageComponent
          message={this.state.notificationMsg}
          pendingWith={this.state.pendingWith}
        />
        {this.state.spinner ? <Spinner /> : null}
        <Router history={history}>
          <div>
            <Route
              exact
              path="/"
              render={() => {
                return this.state.auth ? (
                  this.state.navigate === "admin" ? (
                    <Redirect to="/admin" />
                  ) : (
                    <Redirect to="/interviewer" />
                  )
                ) : (
                  <Signin
                    handleLogin={e => this.handleLogin(e)}
                    navigate={this.state.navigate}
                    auth={this.state.auth}
                    notificationMsg={this.state.notificationMsg}
                    // disableSignupDone={this.disableSignupDone}
                  />
                );
              }}
            />

            <Route
              path="/admin"
              render={() => {
                return this.state.auth ? (
                  <div>
                    <Admin
                      name={this.state.name}
                      id={this.state.id}
                      approverId={this.state.approverId}
                      email={this.state.email}
                      token={this.state.token}
                      auth={this.state.auth}
                      handleLogout={this.handleLogout}
                      handleNotification={msg => this.handleNotification(msg)}
                      clearStorage={() => this.clearStorage()}
                    />
                    <Redirect to="/admin/admin-home" />
                  </div>
                ) : (
                  <Redirect to="/" />
                );
              }}
            />
            <Route
              path="/interviewer"
              render={() => {
                return this.state.auth ? (
                  <div>
                    <Interviewer
                      name={this.state.name}
                      id={this.state.id}
                      email={this.state.email}
                      token={this.state.token}
                      handleLogout={this.handleLogout}
                      handleNotification={msg => this.handleNotification(msg)}
                      clearStorage={() => this.clearStorage()}
                    />
                    <Redirect to="/interviewer/user-home" />
                  </div>
                ) : (
                  <Redirect to="/" />
                );
              }}
            />
            <Route
              exact
              path="/signup"
              render={() => {
                return (
                  <div>
                    {this.state.signupDone ? <Redirect to="/" /> : null}
                    <Signup
                      handleSignup={this.handleSignup}
                      handleChange={(e, name) => {
                        this.captureChange(e, name);
                      }}
                      requestType={this.state.requestType}
                    />
                  </div>
                );
              }}
            />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
